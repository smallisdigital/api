package com.smallisdigital.model;

import jakarta.persistence.*;

@Entity
@Table(name = "function")
public class Function {
    @Id
    @SequenceGenerator(name = "function_id_seq", sequenceName = "function_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "function_id_seq")
    @Column(name = "id", nullable = false, updatable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "userid", referencedColumnName = "id", nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "categoryid", referencedColumnName = "id")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "subcategoryid", referencedColumnName = "id")
    private SubCategory subCategory;

    public Function() {}

    public Function(User user, Category category, SubCategory subCategory) {
        this.user = user;
        this.category = category;
        this.subCategory = subCategory;
    }

    public Integer getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public Category getCategory() {
        return category;
    }

    public SubCategory getSubCategory() {
        return subCategory;
    }
}
