package com.smallisdigital.model;

import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "survey")
public class Survey {
    @Id
    @SequenceGenerator(name = "survey_id_seq", sequenceName = "survey_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="survey_id_seq")
    @Column(name = "id", nullable = false, updatable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "userid", referencedColumnName = "id", nullable = false)
    private User user;

    @Column(name = "creation_date", nullable = false)
    private Date creationDate;

    @Column(name = "status", nullable = false)
    private Integer status;

    @Column(name = "name", columnDefinition = "TEXT", nullable = false)
    private String name;

    @Column(name = "question", columnDefinition = "TEXT", nullable = false)
    private String question;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    public Survey() {}

    public Survey(User user, Date creationDate, Integer status, String name, String question, String description) {
        this.user = user;
        this.creationDate = creationDate;
        this.status = status;
        this.name = name;
        this.question = question;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public String getQuestion() {
        return question;
    }

    public String getDescription() {
        return description;
    }
}
