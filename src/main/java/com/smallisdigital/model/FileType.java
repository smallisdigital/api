package com.smallisdigital.model;

import jakarta.persistence.*;

@Entity
@Table(name = "filetype")
public class FileType {
    @Id
    @SequenceGenerator(name = "filetype_id_seq", sequenceName = "filetype_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="filetype_id_seq")
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "name", length = 32, nullable = false)
    private String name;

    public FileType() {
        /* Do nothing */
    }

    public FileType(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
