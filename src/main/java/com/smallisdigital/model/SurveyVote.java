package com.smallisdigital.model;

import jakarta.persistence.*;

@Entity
@Table(name = "survey_vote")
public class SurveyVote {
    @Id
    @SequenceGenerator(name = "survey_vote_id_seq", sequenceName = "survey_vote_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="survey_vote_id_seq")
    @Column(name = "id", nullable = false, updatable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "surveyId", referencedColumnName = "id", nullable = false)
    private Survey survey;

    @ManyToOne
    @JoinColumn(name = "surveyResponseId", referencedColumnName = "id", nullable = false)
    private SurveyResponse surveyResponse;

    @ManyToOne
    @JoinColumn(name = "userId", referencedColumnName = "id", nullable = false)
    private User user;

    public SurveyVote() {}

    public SurveyVote(Survey survey, SurveyResponse surveyResponse, User user) {
        this.survey = survey;
        this.surveyResponse = surveyResponse;
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public Survey getSurvey() {
        return survey;
    }

    public SurveyResponse getSurveyResponse() {
        return surveyResponse;
    }

    public User getUser() {
        return user;
    }
}
