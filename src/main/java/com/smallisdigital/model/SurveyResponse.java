package com.smallisdigital.model;

import jakarta.persistence.*;

@Entity
@Table(name = "survey_response")
public class SurveyResponse {
    @Id
    @SequenceGenerator(name = "survey_response_id_seq", sequenceName = "survey_response_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="survey_response_id_seq")
    @Column(name = "id", nullable = false, updatable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "surveyId", referencedColumnName = "id", nullable = false)
    private Survey survey;

    @Column(name = "response", nullable = false)
    private String value;

    public SurveyResponse() {}

    public SurveyResponse(Survey survey, String value) {
        this.survey = survey;
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public Survey getSurvey() {
        return survey;
    }

    public String getValue() {
        return value;
    }
}
