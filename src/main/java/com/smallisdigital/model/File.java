package com.smallisdigital.model;

import jakarta.persistence.*;

@Entity
@Table(name = "file")
public class File {
    @Id
    @SequenceGenerator(name = "file_id_seq", sequenceName = "file_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="file_id_seq")
    @Column(name = "id", nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "request", referencedColumnName = "id", nullable = false)
    private Request request;

    @ManyToOne
    @JoinColumn(name = "filetype", referencedColumnName = "id", nullable = false)
    private FileType fileType;

    @Column(name = "data", nullable = false)
    private byte[] data;

    public File() {
        /* Do nothing */
    }

    public File(Request request, FileType fileType, byte[] data) {
        this.request = request;
        this.fileType = fileType;
        this.data = data;
    }

    public Integer getId() {
        return id;
    }

    public Request getRequest() {
        return request;
    }

    public FileType getFileType() {
        return fileType;
    }

    public byte[] getData() {
        return data;
    }
}
