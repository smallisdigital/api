package com.smallisdigital.model;

import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "notation")
public class Notation {
    @Id
    @SequenceGenerator(name = "notation_id_seq", sequenceName = "notation_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "notation_id_seq")
    @Column(name = "id", nullable = false, updatable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "requestid", referencedColumnName = "id", nullable = false)
    private Request request;

    @ManyToOne
    @JoinColumn(name = "userid", referencedColumnName = "id", nullable = false)
    private User user;

    @Column(name = "note", nullable = false)
    private Double note;

    @Column(name = "note_global", nullable = false)
    private Integer noteGlobal;

    @Column(name = "note_delay", nullable = false)
    private Integer noteDelay;

    @Column(name = "note_quality", nullable = false)
    private Integer noteQuality;

    @Column(name = "text", columnDefinition = "TEXT")
    private String text;

    @Column(name = "date", nullable = false)
    private Date date;

    public Notation() {}

    public Notation(Request request, User user, Double note, Integer noteGlobal, Integer noteDelay, Integer noteQuality, String text, Date date) {
        this.request = request;
        this.user = user;
        this.note = note;
        this.noteGlobal = noteGlobal;
        this.noteDelay = noteDelay;
        this.noteQuality = noteQuality;
        this.text = text;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public Request getRequest() {
        return request;
    }

    public User getUser() {
        return user;
    }

    public Double getNote() {
        return note;
    }

    public Integer getNoteGlobal() {
        return noteGlobal;
    }

    public Integer getNoteDelay() {
        return noteDelay;
    }

    public Integer getNoteQuality() {
        return noteQuality;
    }

    public String getText() {
        return text;
    }

    public Date getDate() {
        return date;
    }
}
