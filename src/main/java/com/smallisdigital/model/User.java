package com.smallisdigital.model;

import com.smallisdigital.common.Constants;

import jakarta.persistence.*;

@Entity
@Table(name = "users")
public class User {
    @Id
    @SequenceGenerator(name = "user_id_seq", sequenceName = "user_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="user_id_seq")
    @Column(name = "id", nullable = false, updatable = false)
    private Integer id;

    @Column(name = "firstname", nullable = false, length = Constants.MODEL_USER_FIRSTNAME_MAXLENGTH)
    private String firstname;

    @Column(name = "lastname", nullable = false, length = Constants.MODEL_USER_LASTNAME_MAXLENGTH)
    private String lastname;

    @Column(name = "email", nullable = false, length = Constants.MODEL_USER_EMAIL_MAXLENGTH)
    private String email;

    @Column(name = "password", nullable = false, length = Constants.MODEL_USER_PASSWORD_MAXLENGTH)
    private String password;

    @Column(name = "birthday")
    private Integer birthday;

    @Column(name = "address", length = Constants.MODEL_USER_ADDRESS_MAXLENGTH)
    private String address;

    @Column(name = "phone", length = Constants.MODEL_USER_PHONE_MAXLENGTH)
    private String phone;

    @ManyToOne
    @JoinColumn(name = "level", referencedColumnName = "id", nullable = false)
    private Level level;

    public User() {}

    public User(String firstname, String lastname, String email, String password, Integer birthday, String address, String phone, Level level) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.birthday = birthday;
        this.address = address;
        this.phone = phone;
        this.level = level;
    }

    public Integer getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getAddress() {
        return address;
    }

    public Integer getBirthday() {
        return birthday;
    }

    public String getPhone() {
        return phone;
    }

    public Level getLevel() {
        return level;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setBirthday(Integer birthday) {
        this.birthday = birthday;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
