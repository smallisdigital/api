package com.smallisdigital.model;

import jakarta.persistence.*;

@Entity
@Table(name = "subcategory")
public class SubCategory {
    @Id
    @SequenceGenerator(name = "subcategory_id_seq", sequenceName = "subcategory_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="subcategory_id_seq")
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "name", length = 32, nullable = false)
    private String name;
    
    @ManyToOne
    @JoinColumn(name = "category", referencedColumnName = "id", nullable = false)
    private Category category;

    public SubCategory() {
        /* Do nothing */
    }

    public SubCategory(Integer id, String name, Category category) {
        this.id = id;
        this.name = name;
        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Category getCategory() {
        return category;
    }
}
