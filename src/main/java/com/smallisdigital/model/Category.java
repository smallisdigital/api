package com.smallisdigital.model;

import jakarta.persistence.*;

@Entity
@Table(name = "category")
public class Category {
    @Id
    @SequenceGenerator(name = "category_id_seq", sequenceName = "category_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="category_id_seq")
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "name", length = 32, nullable = false)
    private String name;

    public Category() {
        /* Do nothing */
    }

    public Category(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
