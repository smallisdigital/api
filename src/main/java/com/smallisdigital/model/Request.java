package com.smallisdigital.model;

import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "request")
public class Request {
    @Id
    @SequenceGenerator(name = "request_id_seq", sequenceName = "request_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="request_id_seq")
    @Column(name = "id", nullable = false, updatable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "userid", referencedColumnName = "id", nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "category", referencedColumnName = "id", nullable = false)
    private Category category;

    @ManyToOne
    @JoinColumn(name = "subcategory", referencedColumnName = "id", nullable = false)
    private SubCategory subcategory;

    @Column(name = "address", columnDefinition = "TEXT", nullable = false)
    private String address;

    @Column(name = "description", columnDefinition = "TEXT", nullable = false)
    private String description;

    @Column(name = "creation_date", nullable = false)
    private Date creationDate;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "complete_date")
    private Date completeDate;

    @Column(name = "schedule_date")
    private Date scheduleDate;

    @Column(name = "status", nullable = false)
    private Integer status;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "delay")
    private Integer delay;

    @ManyToOne
    @JoinColumn(name = "currentuser", referencedColumnName = "id")
    private User currentUser;

    public Request() {}

    public Request(User user, Category category, SubCategory subcategory, String address, String description, Date creationDate, Date startDate, Date completeDate, Date scheduleDate, Integer status, Integer priority, Integer delay, User currentUser) {
        this.user = user;
        this.category = category;
        this.subcategory = subcategory;
        this.address = address;
        this.description = description;
        this.creationDate = creationDate;
        this.startDate = startDate;
        this.completeDate = completeDate;
        this.scheduleDate = scheduleDate;
        this.status = status;
        this.priority = priority;
        this.delay = delay;
        this.currentUser = currentUser;
    }

    public Integer getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public Category getCategory() {
        return category;
    }

    public SubCategory getSubcategory() {
        return subcategory;
    }

    public String getAddress() {
        return address;
    }

    public String getDescription() {
        return description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getCompleteDate() {
        return completeDate;
    }

    public void setCompleteDate(Date completeDate) {
        this.completeDate = completeDate;
    }

    public Date getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(Date scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getDelay() {
        return delay;
    }

    public void setDelay(Integer delay) {
        this.delay = delay;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }
}
