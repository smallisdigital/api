package com.smallisdigital.controller;

import com.smallisdigital.dto.CategoryDto;
import com.smallisdigital.dto.SubCategoryDto;
import com.smallisdigital.service.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/category")
public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Operation(summary = "Get categories's list")
    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CategoryDto> getCategoryList() {
        return new ResponseEntity<>(categoryService.getCategoryList(), HttpStatus.OK);
    }

    @Operation(summary = "Get sub categories's list")
    @GetMapping(value = "/sub/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SubCategoryDto> getSubCategoryList(
            @Parameter(description = "Category", required = true)
            @RequestParam("category") Integer category
    ) {
        return new ResponseEntity<>(categoryService.getSubCategoryList(category), HttpStatus.OK);
    }
}
