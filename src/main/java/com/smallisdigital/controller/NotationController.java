package com.smallisdigital.controller;

import com.smallisdigital.dto.AddNotationDto;
import com.smallisdigital.service.NotationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/notation")
public class NotationController {
    private final NotationService notationService;
    public NotationController(NotationService notationService) {
        this.notationService = notationService;
    }

    @Operation(summary = "Create a request")
    @PostMapping(value = "/add", produces = MediaType.APPLICATION_JSON_VALUE)
    public int create(
            @Parameter(description = "Request DTO", required = true)
            @RequestBody AddNotationDto notation
    ) {
        return notationService.add(notation);
    }
}
