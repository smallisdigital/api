package com.smallisdigital.controller;

import com.smallisdigital.service.StatisticService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/statistic")
public class StatisticController {
    private final StatisticService statisticService;

    public StatisticController(StatisticService statisticService) {
        this.statisticService = statisticService;
    }

    @Operation(summary = "Get statistics for request")
    @GetMapping(value = "/request", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Integer>> getRequestByPeriod(
            @Parameter(description = "Period", required = true)
            @RequestParam String period
    ) {
        List<Integer> list = statisticService.requestByPeriod(period);

        HttpStatus httpStatus;

        if(!list.isEmpty())
            httpStatus = HttpStatus.OK;
        else
            httpStatus = HttpStatus.BAD_REQUEST;

        return new ResponseEntity<>(list, httpStatus);
    }
}
