package com.smallisdigital.controller;

import com.smallisdigital.dto.*;
import com.smallisdigital.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Operation(summary = "Get a user by id")
    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GetUserDto> getUser(
            @Parameter(description = "User id", required = true)
            @RequestParam("userId") Integer userId
    ) {
        return new ResponseEntity<>(userService.getUser(userId), HttpStatus.OK);
    }

    @Operation(summary = "Login a user")
    @GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LoginUserDto> loginUser(
            @Parameter(description = "Email", required = true)
            @RequestParam("email") String email,
            @Parameter(description = "Password", required = true)
            @RequestParam("password") String password
    ) {
        LoginUserDto loginUserDto = userService.loginUser(email, password);

        return (loginUserDto != null && loginUserDto.success()) ? new ResponseEntity<>(loginUserDto, HttpStatus.OK) : new ResponseEntity<>(loginUserDto, HttpStatus.FORBIDDEN);
    }

    @Operation(summary = "Create a user")
    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CreateUserStatusDto> createUser(
            @Parameter(description = "User DTO", required = true)
            @RequestBody CreateUserDto user
    ) {
        CreateUserStatusDto createUserStatusDto = userService.createUser(user);

        return (createUserStatusDto != null && createUserStatusDto.success()) ? new ResponseEntity<>(createUserStatusDto, HttpStatus.CREATED) : new ResponseEntity<>(createUserStatusDto, HttpStatus.BAD_REQUEST);
    }

    @Operation(summary = "Update a user")
    @PostMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CreateUserStatusDto> updateUser(
            @Parameter(description = "User DTO", required = true)
            @RequestBody UpdateUserDto user
    ) {
        CreateUserStatusDto updateUserStatusDto = userService.updateUser(user);

        return (updateUserStatusDto != null && updateUserStatusDto.success()) ? new ResponseEntity<>(updateUserStatusDto, HttpStatus.CREATED) : new ResponseEntity<>(updateUserStatusDto, HttpStatus.BAD_REQUEST);
    }
}
