package com.smallisdigital.controller;

import com.smallisdigital.dto.*;
import com.smallisdigital.service.SurveyService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/survey")
public class SurveyController {
    private final SurveyService surveyService;

    public SurveyController(SurveyService surveyService) {
        this.surveyService = surveyService;
    }

    @Operation(summary = "Create a survey")
    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public int create(
            @Parameter(description = "Survey DTO", required = true)
            @RequestBody CreateSurveyDto survey
    ) {
        return surveyService.create(survey);
    }

    @Operation(summary = "Update a survey")
    @PostMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public int update(
            @Parameter(description = "Survey status DTO", required = true)
            @RequestBody UpdateSurveyDto survey
    ) {
        return surveyService.update(survey);
    }

    @Operation(summary = "Vote for a survey")
    @PostMapping(value = "/vote", produces = MediaType.APPLICATION_JSON_VALUE)
    public int vote(
            @Parameter(description = "Survey status DTO", required = true)
            @RequestBody VoteSurveyDto vote
    ) {
        return surveyService.vote(vote);
    }

    @Operation(summary = "Get all survey list")
    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ListSurveyDto>> list(
            @Parameter(description = "User id", required = true)
            @RequestParam Integer userId,
            @Parameter(description = "Contribution", required = true)
            @RequestParam Boolean contribution
    ) {
        return new ResponseEntity<>(surveyService.list(userId, contribution), HttpStatus.OK);
    }
}
