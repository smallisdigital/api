package com.smallisdigital.controller;

import com.smallisdigital.dto.*;
import com.smallisdigital.service.RequestService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/request")
public class RequestController {
    private final RequestService requestService;

    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }

    @Operation(summary = "Create a request")
    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public int create(
            @Parameter(description = "Request DTO", required = true)
            @RequestBody CreateRequestDto request
    ) {
        return requestService.create(request);
    }

    @Operation(summary = "Update a request")
    @PostMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> update(
            @Parameter(description = "Request DTO", required = true)
            @RequestBody UpdateRequestDto request
    ) {
        int status = requestService.update(request);

        HttpStatus httpStatus;

        if(status == 0)
            httpStatus = HttpStatus.OK;
        else
            httpStatus = HttpStatus.BAD_REQUEST;

        return new ResponseEntity<>(status, httpStatus);
    }

    @Operation(summary = "Complete a request")
    @PostMapping(value = "/complete", produces = MediaType.APPLICATION_JSON_VALUE)
    public int update(
            @Parameter(description = "Request DTO", required = true)
            @RequestBody CompleteRequestDto request
    ) {
        return requestService.complete(request);
    }

    @Operation(summary = "Get the request by id")
    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RequestDto> get(
            @Parameter(description = "Input id", required = true)
            @RequestParam Integer inputId
    ) {
        return new ResponseEntity<>(requestService.get(inputId), HttpStatus.OK);
    }

    @Operation(summary = "Get the request list by userId")
    @GetMapping(value = "/list", params = "userId", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ListRequestDto>> listByUser(
            @Parameter(description = "User id", required = true)
            @RequestParam Integer userId
    ) {
        return new ResponseEntity<>(requestService.listByUser(userId), HttpStatus.OK);
    }

    @Operation(summary = "Get the request list by category")
    @GetMapping(value = "/list", params = "subCategoryId", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ListRequestDto>> listBySubCategory(
            @Parameter(description = "Sub category Id", required = true)
            @RequestParam Integer subCategoryId,
            @Parameter(description = "Request status", required = true)
            @RequestParam Integer status
    ) {
        return new ResponseEntity<>(requestService.listBySubCategory(subCategoryId, status), HttpStatus.OK);
    }

    @Operation(summary = "Get the request list by category")
    @GetMapping(value = "/list", params = "inputIds", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ListRequestDto>> listByInputId(
            @Parameter(description = "Input id list", required = true)
            @RequestParam String inputIds
    ) {
        List<ListRequestDto> list = requestService.listByInputId(inputIds);

        HttpStatus httpStatus;

        if(list.isEmpty())
            httpStatus = HttpStatus.OK;
        else
            httpStatus = HttpStatus.BAD_REQUEST;

        return new ResponseEntity<>(list, httpStatus);
    }
}
