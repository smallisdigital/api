package com.smallisdigital.service;

import com.smallisdigital.dto.*;

public interface UserService {
    GetUserDto getUser(Integer userId);
    LoginUserDto loginUser(String email, String password);
    CreateUserStatusDto createUser(CreateUserDto user);
    CreateUserStatusDto updateUser(UpdateUserDto user);
}
