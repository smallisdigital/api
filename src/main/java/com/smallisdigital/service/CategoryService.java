package com.smallisdigital.service;

import com.smallisdigital.dto.CategoryDto;
import com.smallisdigital.dto.SubCategoryDto;

public interface CategoryService {
    CategoryDto getCategoryList();
    SubCategoryDto getSubCategoryList(Integer category);
}
