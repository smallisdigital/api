package com.smallisdigital.service;

import java.util.List;

public interface StatisticService {
    List<Integer> requestByPeriod(String period);
}
