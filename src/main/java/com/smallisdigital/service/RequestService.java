package com.smallisdigital.service;

import com.smallisdigital.dto.*;

import java.util.List;

public interface RequestService {
    int create(CreateRequestDto request);
    int update(UpdateRequestDto request);
    int complete(CompleteRequestDto request);
    RequestDto get(Integer inputId);
    List<ListRequestDto> listByUser(Integer userId);
    List<ListRequestDto> listBySubCategory(Integer subCategoryId, Integer status);
    List<ListRequestDto> listByInputId(String inputIds);
}
