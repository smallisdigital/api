package com.smallisdigital.service;

import com.smallisdigital.dto.AddNotationDto;

public interface NotationService {
    int add(AddNotationDto notation);
}
