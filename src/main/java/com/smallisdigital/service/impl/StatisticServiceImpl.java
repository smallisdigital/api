package com.smallisdigital.service.impl;

import com.smallisdigital.common.Constants;
import com.smallisdigital.model.Request;
import com.smallisdigital.repository.RequestRepository;
import com.smallisdigital.service.StatisticService;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class StatisticServiceImpl implements StatisticService {
    private final RequestRepository requestRepository;

    public StatisticServiceImpl(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    @Override
    public List<Integer> requestByPeriod(String period) {

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy");
        Calendar calendar = Calendar.getInstance();

        if(period.equals(Constants.STATISTIC_PERIOD_DAY)) {
            List<Request> requests = requestRepository.findRequestByDay(sdf1.format(new Date()));

            Integer[] list = Collections.nCopies(31, 0).toArray(new Integer[0]);

            for (Request request : requests) {
                if(request.getScheduleDate() != null) {
                    calendar.setTime(request.getScheduleDate());
                    list[calendar.get(Calendar.DAY_OF_MONTH) - 1]++;
                }
            }

            return Arrays.asList(list);
        } else if(period.equals(Constants.STATISTIC_PERIOD_MONTH)) {
            List<Request> requests = requestRepository.findRequestByMonth(sdf2.format(new Date()));

            Integer[] list = Collections.nCopies(12, 0).toArray(new Integer[0]);

            for (Request request : requests) {
                if(request.getScheduleDate() != null) {
                    calendar.setTime(request.getScheduleDate());
                    list[calendar.get(Calendar.MONTH)]++;
                }
            }

            return Arrays.asList(list);
        }

        return Collections.emptyList();
    }
}
