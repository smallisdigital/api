package com.smallisdigital.service.impl;

import com.smallisdigital.common.Constants;
import com.smallisdigital.dto.*;
import com.smallisdigital.model.Function;
import com.smallisdigital.model.Level;
import com.smallisdigital.model.User;
import com.smallisdigital.repository.FunctionRepository;
import com.smallisdigital.repository.LevelRepository;
import com.smallisdigital.repository.UserRepository;
import com.smallisdigital.service.UserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final LevelRepository groupRepository;
    private final FunctionRepository functionRepository;

    public UserServiceImpl(UserRepository userRepository, LevelRepository groupRepository, FunctionRepository functionRepository) {
        this.userRepository = userRepository;
        this.groupRepository = groupRepository;
        this.functionRepository = functionRepository;
    }

    @Override
    public GetUserDto getUser(Integer userId) {
        User user = userRepository.findUserById(userId);

        return (user != null) ? new GetUserDto(user.getFirstname(), user.getLastname(), user.getLevel().getName(), user.getEmail(), user.getPhone(), user.getBirthday(), user.getAddress()) : null;
    }

    @Override
    public LoginUserDto loginUser(String email, String password) {
        User user = userRepository.findByEmailAndPassword(email, password);
        Function function = null;

        if(user != null && user.getLevel().getId() == 2)
            function = functionRepository.findFunctionByUser(user);

        if(function != null)
            return new LoginUserDto(true, user.getId(), user.getFirstname(), user.getLastname(), user.getLevel().getId(), function.getCategory().getId(), function.getCategory().getName(), function.getSubCategory().getId(), function.getSubCategory().getName(), null);
        else if(user != null)
            return new LoginUserDto(true, user.getId(), user.getFirstname(), user.getLastname(), user.getLevel().getId(), null, null, null, null, null);
        else
            return new LoginUserDto(false, null, null,null, null, null, null, null, null, "Les informations de connexions sont erronées");
    }

    @Override
    public CreateUserStatusDto createUser(CreateUserDto user) {
        List<CreateUserErrorDto> error = new ArrayList<>();

        if(user.firstname() == null)
            error.add(new CreateUserErrorDto(Constants.USER_FIRSTNAME, Constants.ERROR_USER_FIRSTNAME_UNSPECIFIED));
        else if(user.firstname().trim().length() > Constants.MODEL_USER_FIRSTNAME_MAXLENGTH)
            error.add(new CreateUserErrorDto(Constants.USER_FIRSTNAME, String.format(Constants.ERROR_USER_FIRSTNAME_MAXLENGTH, Constants.MODEL_USER_FIRSTNAME_MAXLENGTH)));

        if(user.lastname() == null)
            error.add(new CreateUserErrorDto(Constants.USER_LASTNAME, Constants.ERROR_USER_LASTNAME_UNSPECIFIED));
        else if(user.lastname().trim().length() > Constants.MODEL_USER_LASTNAME_MAXLENGTH)
            error.add(new CreateUserErrorDto(Constants.USER_LASTNAME, String.format(Constants.ERROR_USER_LASTNAME_MAXLENGTH, Constants.MODEL_USER_LASTNAME_MAXLENGTH)));

        if(user.email() == null)
            error.add(new CreateUserErrorDto(Constants.USER_EMAIL, Constants.ERROR_USER_EMAIL_UNSPECIFIED));
        else if(user.email().trim().length() > Constants.MODEL_USER_EMAIL_MAXLENGTH)
            error.add(new CreateUserErrorDto(Constants.USER_EMAIL, String.format(Constants.ERROR_USER_EMAIL_MAXLENGTH, Constants.MODEL_USER_EMAIL_MAXLENGTH)));

        if(user.password() == null)
            error.add(new CreateUserErrorDto(Constants.USER_PASSWORD, Constants.ERROR_USER_PASSWORD_UNSPECIFIED));
        else if(user.password().length() < Constants.MODEL_USER_PASSWORD_MINLENGTH)
            error.add(new CreateUserErrorDto(Constants.USER_PASSWORD, String.format(Constants.ERROR_USER_PASSWORD_MINLENGTH, Constants.MODEL_USER_PASSWORD_MINLENGTH)));
        else if(user.password().length() > Constants.MODEL_USER_PASSWORD_MAXLENGTH)
            error.add(new CreateUserErrorDto(Constants.USER_PASSWORD, String.format(Constants.ERROR_USER_PASSWORD_MAXLENGTH, Constants.MODEL_USER_PASSWORD_MAXLENGTH)));
        else if(!user.password().equals(user.repassword()))
            error.add(new CreateUserErrorDto(Constants.USER_PASSWORD, Constants.ERROR_USER_PASSWORD_DIFFERENT));

        if(error.isEmpty()) {
            User newUser = userRepository.findUserByEmail(user.email().trim());

            if(newUser != null && newUser.getEmail() != null) {
                error.add(new CreateUserErrorDto(Constants.USER_EMAIL, Constants.ERROR_USER_EMAIL_EXIST));
            } else {
                Level group = groupRepository.findByName("Citoyen");
                userRepository.save(new User(user.firstname().trim(), user.lastname().trim(), user.email().trim(), user.password(), null, null, null, group));

                return new CreateUserStatusDto(true, null);
            }
        }

        return new CreateUserStatusDto(false, error);
    }

    @Override
    public CreateUserStatusDto updateUser(UpdateUserDto user) {
        List<CreateUserErrorDto> error = new ArrayList<>();

        Integer birthday = null;

        if(user.firstname() == null)
            error.add(new CreateUserErrorDto(Constants.USER_FIRSTNAME, Constants.ERROR_USER_FIRSTNAME_UNSPECIFIED));
        else if(user.firstname().trim().length() > Constants.MODEL_USER_FIRSTNAME_MAXLENGTH)
            error.add(new CreateUserErrorDto(Constants.USER_FIRSTNAME, String.format(Constants.ERROR_USER_FIRSTNAME_MAXLENGTH, Constants.MODEL_USER_FIRSTNAME_MAXLENGTH)));

        if(user.lastname() == null)
            error.add(new CreateUserErrorDto(Constants.USER_LASTNAME, Constants.ERROR_USER_LASTNAME_UNSPECIFIED));
        else if(user.lastname().trim().length() > Constants.MODEL_USER_LASTNAME_MAXLENGTH)
            error.add(new CreateUserErrorDto(Constants.USER_LASTNAME, String.format(Constants.ERROR_USER_LASTNAME_MAXLENGTH, Constants.MODEL_USER_LASTNAME_MAXLENGTH)));

        if(user.phone() != null && user.phone().trim().length() > Constants.MODEL_USER_PHONE_MAXLENGTH)
            error.add(new CreateUserErrorDto(Constants.USER_PHONE, String.format(Constants.ERROR_USER_PHONE_MAXLENGTH, Constants.MODEL_USER_PHONE_MAXLENGTH)));

        if(user.birthday() != null && !user.birthday().isEmpty()) {
            try {
                birthday = Integer.parseInt(user.birthday());
            } catch (Exception ignored) {
                error.add(new CreateUserErrorDto(Constants.USER_BIRTHDAY, Constants.ERROR_USER_BIRTHDAY_INVALID));
            }
        }

        if(user.address() != null && user.address().trim().length() > Constants.MODEL_USER_ADDRESS_MAXLENGTH)
            error.add(new CreateUserErrorDto(Constants.USER_ADDRESS, String.format(Constants.ERROR_USER_EMAIL_MAXLENGTH, Constants.MODEL_USER_ADDRESS_MAXLENGTH)));

        if(error.isEmpty()) {
            User newUser = userRepository.findUserByEmail(user.email().trim());

            if(newUser == null || newUser.getEmail() == null) {
                error.add(new CreateUserErrorDto(Constants.USER_EMAIL, Constants.ERROR_USER_EMAIL_NOTFOUND));
            } else {
                newUser.setFirstname(!user.firstname().isEmpty() ? user.firstname().trim() : null);
                newUser.setLastname(!user.lastname().isEmpty() ? user.lastname().trim() : null);
                newUser.setPhone((user.phone() != null && !user.phone().isEmpty()) ? user.phone().trim() : null);
                newUser.setBirthday(birthday);
                newUser.setAddress((user.address() != null && !user.address().isEmpty()) ? user.address().trim() : null);

                userRepository.save(newUser);

                return new CreateUserStatusDto(true, null);
            }
        }

        return new CreateUserStatusDto(false, error);
    }
}
