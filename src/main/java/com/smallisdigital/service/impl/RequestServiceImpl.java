package com.smallisdigital.service.impl;

import com.smallisdigital.dto.*;
import com.smallisdigital.model.*;
import com.smallisdigital.repository.*;
import com.smallisdigital.service.RequestService;
import com.smallisdigital.common.Constants;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class RequestServiceImpl implements RequestService {
    private final RequestRepository requestRepository;
    private final UserRepository userRepository;
    private final CategoryRepository categoryRepository;
    private final SubCategoryRepository subCategoryRepository;
    private final NotationRepository notationRepository;
    private final FileRepository fileRepository;
    private final FileTypeRepository fileTypeRepository;

    public RequestServiceImpl(RequestRepository requestRepository, UserRepository userRepository, CategoryRepository categoryRepository, SubCategoryRepository subCategoryRepository, NotationRepository notationRepository, FileRepository fileRepository, FileTypeRepository fileTypeRepository) {
        this.userRepository = userRepository;
        this.requestRepository = requestRepository;
        this.categoryRepository = categoryRepository;
        this.subCategoryRepository = subCategoryRepository;
        this.notationRepository = notationRepository;
        this.fileRepository = fileRepository;
        this.fileTypeRepository = fileTypeRepository;
    }

    @Override
    public int create(CreateRequestDto request) {
        User user = userRepository.findUserById(request.user());
        Category category = categoryRepository.findCategoryById(request.category());
        SubCategory subCategory = subCategoryRepository.findSubCategoryById(request.subcategory());
        FileType fileType = fileTypeRepository.findFileTypeByName("Image");

        byte[] image = (request.image() != null) ? Base64.getDecoder().decode(request.image()) : null;

        Request r = requestRepository.save(new Request(user, category, subCategory, request.address(), request.description(), new Date(), null, null, null, 1, null, null, null));

        if(image != null)
            fileRepository.save(new File(r, fileType, image));

        return 0;
    }

    @Override
    public int update(UpdateRequestDto request) {
        User user = userRepository.findUserById(request.userId());
        Request newRequest = requestRepository.findRequestById(request.inputId());

        Date date;

        try {
            date = new SimpleDateFormat("dd/MM/yyyy").parse(request.scheduleDate());
        } catch (ParseException ignored) {
            return 1;
        }

        newRequest.setStartDate(new Date());
        newRequest.setStatus(Constants.REQUEST_RUNNING);
        newRequest.setScheduleDate(date);
        newRequest.setPriority(request.priority());
        newRequest.setDelay(request.delay());
        newRequest.setCurrentUser(user);

        requestRepository.save(newRequest);

        return 0;
    }

    @Override
    public int complete(CompleteRequestDto request) {
        Request newRequest = requestRepository.findRequestById(request.inputId());

        newRequest.setStatus(Constants.REQUEST_COMPLETED);
        newRequest.setCompleteDate(new Date());

        requestRepository.save(newRequest);

        return 0;
    }

    @Override
    public RequestDto get(Integer inputId) {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.SHORT_DATE_FR_PATTERN);
        Request request = requestRepository.findRequestById(inputId);
        Notation notation = notationRepository.findNotationByRequest(request);
        File file = fileRepository.findFileByRequest(request);

        String data = (file != null && file.getData() != null) ? Arrays.toString(Base64.getEncoder().encode(file.getData())) : null;

        String startDate = (request.getStartDate() != null) ? sdf.format(request.getStartDate()) : null;
        String scheduleDate = (request.getScheduleDate() != null) ? sdf.format(request.getScheduleDate()) : null;
        String completeDate = (request.getCompleteDate() != null) ? sdf.format(request.getCompleteDate()) : null;

        return new RequestDto(request.getId(), request.getCategory().getName(), request.getSubcategory().getName(), sdf.format(request.getCreationDate()), startDate, scheduleDate, completeDate, data, request.getAddress(), request.getDescription(), request.getStatus(), request.getPriority(), request.getDelay(), ((notation != null) ? notation.getNote() : null), ((notation != null) ? notation.getNoteGlobal() : null),  ((notation != null) ? notation.getNoteDelay() : null),  ((notation != null) ? notation.getNoteQuality() : null));
    }

    @Override
    public List<ListRequestDto> listByUser(Integer userId) {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.SHORT_DATE_FR_PATTERN);
        List<Request> requests = requestRepository.findRequestByUserId(userId);
        List<ListRequestDto> requestDto = new ArrayList<>();

        for(Request r : requests) {
            Notation notation = notationRepository.findNotationByRequest(r);

            String scheduleDate = (r.getScheduleDate() != null) ? sdf.format(r.getScheduleDate()) : null;

            requestDto.add(new ListRequestDto(r.getId(), r.getCategory().getName(), r.getSubcategory().getName(), sdf.format(r.getCreationDate()), scheduleDate, r.getAddress(), r.getPriority(), r.getDelay(), r.getStatus(), (notation != null)));
        }

        return requestDto;
    }

    @Override
    public List<ListRequestDto> listBySubCategory(Integer subCategoryId, Integer status) {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.SHORT_DATE_FR_PATTERN);

        List<Request> requests = requestRepository.findRequestBySubcategoryIdAndStatus(subCategoryId, status);

        List<ListRequestDto> requestDto = new ArrayList<>();

        for(Request r : requests) {
            Notation notation = notationRepository.findNotationByRequest(r);

            String scheduleDate = (r.getScheduleDate() != null) ? sdf.format(r.getScheduleDate()) : null;

            requestDto.add(new ListRequestDto(r.getId(), r.getCategory().getName(), r.getSubcategory().getName(), sdf.format(r.getCreationDate()), scheduleDate, r.getAddress(), r.getPriority(), r.getDelay(), status, (notation != null)));
        }

        return requestDto;
    }

    @Override
    public List<ListRequestDto> listByInputId(String inputIds) {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.SHORT_DATE_FR_PATTERN);

        List<ListRequestDto> requestDto = new ArrayList<>();

        String[] inputIdList = inputIds.split(",");

        int inputId;

        for(String sInputId : inputIdList) {
            try {
                inputId = Integer.parseInt(sInputId);
            } catch (NumberFormatException ignored) {
                return Collections.emptyList();
            }

            Request r = requestRepository.findRequestById(inputId);
            Notation notation = notationRepository.findNotationByRequest(r);

            String scheduleDate = (r.getScheduleDate() != null) ? sdf.format(r.getScheduleDate()) : null;

            requestDto.add(new ListRequestDto(r.getId(), r.getCategory().getName(), r.getSubcategory().getName(), sdf.format(r.getCreationDate()), scheduleDate, r.getAddress(), r.getPriority(), r.getDelay(), r.getStatus(), (notation != null)));
        }

        return requestDto;
    }
}
