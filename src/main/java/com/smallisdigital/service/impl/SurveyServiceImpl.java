package com.smallisdigital.service.impl;

import com.smallisdigital.dto.*;
import com.smallisdigital.model.*;
import com.smallisdigital.repository.SurveyRepository;
import com.smallisdigital.repository.SurveyResponseRepository;
import com.smallisdigital.repository.SurveyVoteRepository;
import com.smallisdigital.repository.UserRepository;
import com.smallisdigital.service.SurveyService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class SurveyServiceImpl implements SurveyService {
    private final SurveyRepository surveyRepository;
    private final SurveyResponseRepository surveyResponseRepository;
    private final SurveyVoteRepository surveyVoteRepository;
    private final UserRepository userRepository;

    public SurveyServiceImpl(SurveyRepository surveyRepository, SurveyResponseRepository surveyResponseRepository, SurveyVoteRepository surveyVoteRepository, UserRepository userRepository) {
        this.surveyRepository = surveyRepository;
        this.surveyResponseRepository = surveyResponseRepository;
        this.surveyVoteRepository = surveyVoteRepository;
        this.userRepository = userRepository;
    }

    @Override
    public int create(CreateSurveyDto survey) {
        User user = userRepository.findUserById(survey.user());

        Survey s = surveyRepository.save(new Survey(user, new Date(), 1, survey.name(), survey.question(), survey.description()));

        for(String response : survey.response())
            surveyResponseRepository.save(new SurveyResponse(s, response));

        return 0;
    }

    @Override
    public int update(UpdateSurveyDto survey) {
        Survey newSurvey = surveyRepository.findSurveyById(survey.surveyId());

        newSurvey.setStatus(survey.status());

        surveyRepository.save(newSurvey);

        return 0;
    }

    @Override
    public int vote(VoteSurveyDto vote) {
        Survey survey = surveyRepository.findSurveyById(vote.surveyId());
        SurveyResponse surveyResponse = surveyResponseRepository.findSurveyResponseById(vote.surveyResponseId());
        User user = userRepository.findUserById(vote.userId());

        surveyVoteRepository.save(new SurveyVote(survey, surveyResponse, user));

        return 0;
    }

    @Override
    public List<ListSurveyDto> list(Integer userId, Boolean contribution) {
        List<Survey> surveys;

        if(contribution != null && contribution)
            surveys = surveyRepository.findSurveyByStatus(1);
        else
            surveys = surveyRepository.findSurveyByUserIdOrderByStatusAscCreationDateDesc(userId);

        List<ListSurveyDto> surveyDto = new ArrayList<>();

        for(Survey s : surveys) {
            Integer ct = null;

            List<SurveyResponse> surveyResponses = surveyResponseRepository.findSurveyResponseBySurveyId(s.getId());
            List<SurveyVote> surveyVotes = surveyVoteRepository.findSurveyVoteBySurveyId(s.getId());

            List<String> surveyResponseList = new ArrayList<>();
            List<Float> surveyVoteList = new ArrayList<>();

            for(SurveyResponse surveyResponse : surveyResponses) {
                List<SurveyVote> surveyVotes1 = surveyVoteRepository.findSurveyVoteBySurveyResponseId(surveyResponse.getId());

                surveyResponseList.add(surveyResponse.getId() + ";" + surveyResponse.getValue());

                if(!surveyVotes.isEmpty())
                    surveyVoteList.add((float) surveyVotes1.size() / surveyVotes.size() * 100);
                else
                    surveyVoteList.add((float) 0);
            }

            if(contribution != null && contribution) {
                for (SurveyVote vote : surveyVotes) {
                    if (Objects.equals(vote.getUser().getId(), userId)) {
                        ct = vote.getSurveyResponse().getId();
                        break;
                    }
                }
            }

            surveyDto.add(new ListSurveyDto(s.getId(), s.getName(), s.getQuestion(), surveyResponseList, surveyVoteList, s.getStatus(), ct));
        }

        return surveyDto;
    }
}
