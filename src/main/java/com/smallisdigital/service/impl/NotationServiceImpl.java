package com.smallisdigital.service.impl;

import com.smallisdigital.dto.AddNotationDto;
import com.smallisdigital.model.Notation;
import com.smallisdigital.model.Request;
import com.smallisdigital.model.User;
import com.smallisdigital.repository.*;
import com.smallisdigital.service.NotationService;
import org.springframework.stereotype.Service;
import java.util.Date;

@Service
public class NotationServiceImpl implements NotationService {
    private final NotationRepository notationRepository;
    private final RequestRepository requestRepository;
    private final UserRepository userRepository;


    public NotationServiceImpl(NotationRepository notationRepository, RequestRepository requestRepository, UserRepository userRepository) {
        this.notationRepository = notationRepository;
        this.userRepository = userRepository;
        this.requestRepository = requestRepository;
    }

    @Override
    public int add(AddNotationDto notation) {
        Request request = requestRepository.findRequestById(notation.inputId());
        User user = userRepository.findUserById(notation.userId());

        double note = (notation.noteGlobal() + notation.noteDelay() + notation.noteQuality()) / 3.0;

        notationRepository.save(new Notation(request, user, Math.round(note * 100.0) / 100.0, notation.noteGlobal(), notation.noteDelay(), notation.noteQuality(), notation.comment(), new Date()));

        return 0;
    }
}
