package com.smallisdigital.service.impl;

import com.smallisdigital.dto.CategoryDto;
import com.smallisdigital.dto.SubCategoryDto;
import com.smallisdigital.model.Category;
import com.smallisdigital.model.SubCategory;
import com.smallisdigital.repository.CategoryRepository;
import com.smallisdigital.repository.SubCategoryRepository;
import com.smallisdigital.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;
    private final SubCategoryRepository subCategoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository, SubCategoryRepository subCategoryRepository) {
        this.categoryRepository = categoryRepository;
        this.subCategoryRepository = subCategoryRepository;
    }

    @Override
    public CategoryDto getCategoryList() {
        List<Category> category = categoryRepository.findAll();

        return (category != null) ? new CategoryDto(true, category) : null;
    }

    @Override
    public SubCategoryDto getSubCategoryList(Integer category) {
        List<SubCategory> subCategory = subCategoryRepository.findSubCategoryByCategory(new Category(category, null));

        return (subCategory != null) ? new SubCategoryDto(true, subCategory) : null;
    }
}
