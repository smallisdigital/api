package com.smallisdigital.service;

import com.smallisdigital.dto.CreateSurveyDto;
import com.smallisdigital.dto.ListSurveyDto;
import com.smallisdigital.dto.UpdateSurveyDto;
import com.smallisdigital.dto.VoteSurveyDto;

import java.util.List;

public interface SurveyService {
    int create(CreateSurveyDto survey);
    int update(UpdateSurveyDto survey);
    int vote(VoteSurveyDto vote);
    List<ListSurveyDto> list(Integer userId, Boolean contribution);
}
