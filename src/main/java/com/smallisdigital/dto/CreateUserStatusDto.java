package com.smallisdigital.dto;

import java.util.List;

public record CreateUserStatusDto(Boolean success, List<CreateUserErrorDto> error) { }
