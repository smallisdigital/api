package com.smallisdigital.dto;

public record ListRequestDto(int inputId, String category, String subcategory, String creationDate, String scheduleDate, String address, Integer priority, Integer delay, int status, Boolean note) { }
