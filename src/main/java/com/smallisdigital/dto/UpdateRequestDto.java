package com.smallisdigital.dto;

public record UpdateRequestDto(int inputId, int userId, int priority, int delay, String scheduleDate) { }
