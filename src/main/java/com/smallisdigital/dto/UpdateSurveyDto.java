package com.smallisdigital.dto;

public record UpdateSurveyDto(int surveyId, int status) { }
