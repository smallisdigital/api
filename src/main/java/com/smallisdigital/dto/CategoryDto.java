package com.smallisdigital.dto;

import com.smallisdigital.model.Category;

import java.util.List;

public record CategoryDto(Boolean success, List<Category> category) { }
