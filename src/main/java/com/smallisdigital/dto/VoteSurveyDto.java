package com.smallisdigital.dto;

public record VoteSurveyDto(int surveyId, int surveyResponseId, int userId) { }
