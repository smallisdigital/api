package com.smallisdigital.dto;

import com.smallisdigital.model.SubCategory;

import java.util.List;

public record SubCategoryDto(Boolean success, List<SubCategory> subCategories) {}
