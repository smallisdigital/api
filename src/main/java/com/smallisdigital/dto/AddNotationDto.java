package com.smallisdigital.dto;

public record AddNotationDto(int inputId, int userId, int noteGlobal, int noteDelay, int noteQuality, String comment) { }
