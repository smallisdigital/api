package com.smallisdigital.dto;

public record CreateUserDto(String firstname, String lastname, String email, String password, String repassword) { }
