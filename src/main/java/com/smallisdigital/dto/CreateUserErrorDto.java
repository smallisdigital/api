package com.smallisdigital.dto;

public record CreateUserErrorDto(String name, String value) { }
