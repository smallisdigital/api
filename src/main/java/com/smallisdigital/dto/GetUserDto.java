package com.smallisdigital.dto;

public record GetUserDto(String firstname, String lastname, String level, String email, String phone, Integer birthday, String address) { }
