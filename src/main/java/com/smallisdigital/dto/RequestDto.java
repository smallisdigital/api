package com.smallisdigital.dto;

public record RequestDto(int inputId, String category, String subcategory, String creationDate, String startDate, String scheduleDate, String completeDate, String image, String address, String description, int status, Integer priority, Integer delay, Double note, Integer noteGlobal, Integer noteDelay, Integer noteQuality) { }
