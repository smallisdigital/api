package com.smallisdigital.dto;

public record CompleteRequestDto(int inputId) { }
