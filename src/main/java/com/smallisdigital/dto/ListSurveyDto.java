package com.smallisdigital.dto;

import java.util.List;

public record ListSurveyDto(int surveyId, String name, String question, List<String> response, List<Float> vote, int status, Integer contribution) { }
