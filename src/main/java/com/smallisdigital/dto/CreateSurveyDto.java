package com.smallisdigital.dto;

import java.util.List;

public record CreateSurveyDto(int user, String name, String question, String description, List<String> response) { }
