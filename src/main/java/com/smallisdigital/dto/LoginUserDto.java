package com.smallisdigital.dto;

public record LoginUserDto(Boolean success, Integer id, String firstname, String lastname, Integer level, Integer categoryId, String categoryName, Integer subCategoryId, String subCategoryName, String error) { }
