package com.smallisdigital.dto;

public record CreateRequestDto(int user, int category, int subcategory, String address, String description, String image) { }
