package com.smallisdigital.dto;

public record UpdateUserDto(String firstname, String lastname, String email, String phone, String birthday, String address) { }
