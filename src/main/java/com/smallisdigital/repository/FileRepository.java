package com.smallisdigital.repository;

import com.smallisdigital.model.File;
import com.smallisdigital.model.Request;
import org.springframework.data.repository.CrudRepository;

public interface FileRepository extends CrudRepository<File, Integer> {
    File findFileByRequest(Request request);
}
