package com.smallisdigital.repository;

import com.smallisdigital.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    User findUserById(Integer id);
    User findUserByEmail(String email);
    User findByEmailAndPassword(String email, String password);
}
