package com.smallisdigital.repository;

import com.smallisdigital.model.FileType;
import org.springframework.data.repository.CrudRepository;

public interface FileTypeRepository extends CrudRepository<FileType, Integer> {
    FileType findFileTypeByName(String name);
}
