package com.smallisdigital.repository;

import com.smallisdigital.model.Request;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RequestRepository extends CrudRepository<Request, Integer> {
    List<Request> findRequestByUserId(Integer userId);
    List<Request> findRequestBySubcategoryIdAndStatus(Integer subCategoryId, Integer status);
    Request findRequestById(Integer inputId);

    @Query(value = "SELECT * FROM request WHERE TO_CHAR(schedule_date, 'YYYY-MM') = ?1", nativeQuery = true)
    List<Request> findRequestByDay(@Param("scheduleDate") String scheduleDate);

    @Query(value = "SELECT * FROM request WHERE TO_CHAR(schedule_date, 'YYYY') = ?1", nativeQuery = true)
    List<Request> findRequestByMonth(@Param("scheduleDate") String scheduleDate);
}
