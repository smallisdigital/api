package com.smallisdigital.repository;

import com.smallisdigital.model.Function;
import com.smallisdigital.model.User;
import org.springframework.data.repository.CrudRepository;

public interface FunctionRepository extends CrudRepository<Function, User> {
    Function findFunctionByUser(User user);
}
