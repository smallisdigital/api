package com.smallisdigital.repository;

import com.smallisdigital.model.Level;
import org.springframework.data.repository.CrudRepository;

public interface LevelRepository extends CrudRepository<Level, Integer> {
    Level findByName(String name);
}
