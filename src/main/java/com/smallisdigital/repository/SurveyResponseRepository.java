package com.smallisdigital.repository;

import com.smallisdigital.model.SurveyResponse;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SurveyResponseRepository extends CrudRepository<SurveyResponse, Integer> {
    SurveyResponse findSurveyResponseById(int surveyResponseId);
    List<SurveyResponse> findSurveyResponseBySurveyId(int surveyId);
}
