package com.smallisdigital.repository;

import com.smallisdigital.model.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CategoryRepository extends CrudRepository<Category, Integer> {
    List<Category> findAll();
    Category findCategoryById(Integer id);
}
