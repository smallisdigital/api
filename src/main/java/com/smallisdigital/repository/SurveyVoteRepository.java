package com.smallisdigital.repository;

import com.smallisdigital.model.SurveyVote;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SurveyVoteRepository extends CrudRepository<SurveyVote, Integer> {
    List<SurveyVote> findSurveyVoteBySurveyId(Integer surveyId);
    List<SurveyVote> findSurveyVoteBySurveyResponseId(Integer surveyResponseId);
}
