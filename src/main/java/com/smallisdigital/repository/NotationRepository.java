package com.smallisdigital.repository;

import com.smallisdigital.model.Notation;
import com.smallisdigital.model.Request;
import org.springframework.data.repository.CrudRepository;

public interface NotationRepository extends CrudRepository<Notation, Integer> {
    Notation findNotationByRequest(Request request);
}
