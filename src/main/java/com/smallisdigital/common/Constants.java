package com.smallisdigital.common;

public class Constants {
    
    private Constants() {
        throw new IllegalStateException("Constants class");
    }

    public static final int MODEL_USER_FIRSTNAME_MAXLENGTH = 32;
    public static final int MODEL_USER_LASTNAME_MAXLENGTH = 32;
    public static final int MODEL_USER_EMAIL_MAXLENGTH = 128;
    public static final int MODEL_USER_PASSWORD_MINLENGTH = 6;
    public static final int MODEL_USER_PASSWORD_MAXLENGTH = 128;
    public static final int MODEL_USER_ADDRESS_MAXLENGTH = 512;
    public static final int MODEL_USER_PHONE_MAXLENGTH = 10;

    public static final int REQUEST_TODO = 1;
    public static final int REQUEST_RUNNING = 2;
    public static final int REQUEST_COMPLETED = 3;

    public static final String SHORT_DATE_FR_PATTERN = "dd/MM/yyyy";

    public static final String USER_FIRSTNAME = "firstname";
    public static final String USER_LASTNAME = "lastname";
    public static final String USER_EMAIL = "email";
    public static final String USER_PASSWORD = "password";
    public static final String USER_PHONE = "phone";
    public static final String USER_BIRTHDAY = "birthday";
    public static final String USER_ADDRESS = "address";

    public static final String ERROR_USER_FIRSTNAME_UNSPECIFIED = "Le prénom doit être renseigné";
    public static final String ERROR_USER_FIRSTNAME_MAXLENGTH = "Le prénom doit comporter au maximum %d caractères";
    public static final String ERROR_USER_LASTNAME_UNSPECIFIED = "Le nom doit être renseigné";
    public static final String ERROR_USER_LASTNAME_MAXLENGTH = "Le nom doit comporter au maximum %d caractères";
    public static final String ERROR_USER_EMAIL_UNSPECIFIED = "L'adresse mail doit être renseignée";
    public static final String ERROR_USER_EMAIL_MAXLENGTH = "L'adresse mail doit comporter au maximum %d caractères";
    public static final String ERROR_USER_EMAIL_EXIST = "Cette adresse email est déjà utilisée pour un autre compte";
    public static final String ERROR_USER_EMAIL_NOTFOUND = "Impossible de trouver l'utilisateur avec l'adresse email renseignée";
    public static final String ERROR_USER_PASSWORD_UNSPECIFIED = "Le mot de passe doit être renseigné";
    public static final String ERROR_USER_PASSWORD_MINLENGTH = "Le mot de passe doit comporter au minimum %d caractères";
    public static final String ERROR_USER_PASSWORD_MAXLENGTH = "Le mot de passe doit comporter au maximum %d caractères";
    public static final String ERROR_USER_PASSWORD_DIFFERENT = "Les mots de passes saisis ne sont pas identiques";
    public static final String ERROR_USER_PHONE_MAXLENGTH = "Le numéro de téléphone doit comporter au maximum %d caractères";
    public static final String ERROR_USER_BIRTHDAY_INVALID = "La date de naissance n'est pas une date valide";

    public static final String STATISTIC_PERIOD_DAY = "day";
    public static final String STATISTIC_PERIOD_MONTH = "month";
}
