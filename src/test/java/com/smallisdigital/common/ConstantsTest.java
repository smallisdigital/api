package com.smallisdigital.common;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ConstantsTest {
    @Test
    void ConstantsConstructorTest() {
        Throwable currentException = null;

        try {
            Constructor<Constants> constructor =  Constants.class.getDeclaredConstructor();

            assertTrue(Modifier.isPrivate(constructor.getModifiers()));

            constructor.setAccessible(true);
            constructor.newInstance();
        } catch (Exception e) {
            currentException = e;
        }

        assertTrue(currentException instanceof InvocationTargetException);
    }
}
