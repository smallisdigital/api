package com.smallisdigital.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.smallisdigital.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserController.class)
class UserControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Test
    void getUser() throws Exception {
        mockMvc.perform(get("/api/user/?userId=1")).andExpect(status().isOk());
    }

    @Test
    void loginUser() throws Exception {
        mockMvc.perform(get("/api/user/login?email=contact@smallisdigital.com&password=smallisdigital")).andExpect(status().isForbidden());
    }

    @Test
    void createUser() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        ObjectNode node = mapper.createObjectNode();
        node.put("firstname", "Small is Digital");
        node.put("lastname", "Admin");
        node.put("email", "contact@smallisdigital.com");
        node.put("password", "smallisdigital");
        node.put("repassword", "smallisdigital");

        String json = mapper.writeValueAsString(node);

        mockMvc.perform(post("/api/user/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    void updateUser() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        ObjectNode node = mapper.createObjectNode();
        node.put("firstname", "Small is Digital");
        node.put("lastname", "Admin");
        node.put("email", "contact@smallisdigital.com");
        node.put("birthday", "1970");
        node.put("phone", "0102030405");
        node.put("address", "4954 Par Drive");

        String json = mapper.writeValueAsString(node);

        mockMvc.perform(post("/api/user/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isBadRequest());
    }
}