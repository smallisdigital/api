package com.smallisdigital.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.smallisdigital.service.RequestService;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = RequestController.class)
class RequestControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private RequestService requestService;

    @Test
    void create() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        ObjectNode node = mapper.createObjectNode();
        node.put("user", "1");
        node.put("category", "1");
        node.put("subcategory", "1");
        node.put("address", "4954 Par Drive");
        node.put("description", "New content");

        String json = mapper.writeValueAsString(node);

        mockMvc.perform(post("/api/request/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk());
    }

    @Test
    void update() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        ObjectNode node = mapper.createObjectNode();
        node.put("inputId", "1");
        node.put("userId", "1");
        node.put("priority", "1");
        node.put("delay", "1");

        String json = mapper.writeValueAsString(node);

        mockMvc.perform(post("/api/request/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isOk());
    }

    @Test
    void complete() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        ObjectNode node = mapper.createObjectNode();
        node.put("inputId", "1");

        String json = mapper.writeValueAsString(node);

        mockMvc.perform(post("/api/request/complete")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isOk());
    }

    @Test
    void get() throws Exception {
        mockMvc.perform(org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get("/api/request/?inputId=1")).andExpect(status().isOk());
    }

    @Test
    void listByUser() throws Exception {
        mockMvc.perform(org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get("/api/request/list?userId=1")).andExpect(status().isOk());
    }

    @Test
    void listBySubCategory() throws Exception {
        mockMvc.perform(org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get("/api/request/list?subCategoryId=1&status=1")).andExpect(status().isOk());
    }

    @Test
    void listByInputId() throws Exception {
        mockMvc.perform(org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get("/api/request/list?inputIds=1,2,3,4,5")).andExpect(status().isOk());
    }
}