package com.smallisdigital.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.smallisdigital.service.SurveyService;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = SurveyController.class)
class SurveyControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private SurveyService surveyService;

    @Test
    void create() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        ObjectNode node = mapper.createObjectNode();
        node.put("user", "1");
        node.put("name", "Survey's name");
        node.put("question", "How is a survey ?");
        node.put("description", "Survey's description");

        ArrayNode arrayNode = mapper.createArrayNode();
        arrayNode.add("Survey's response 1");
        arrayNode.add("Survey's response 2");

        node.set("response", arrayNode);

        String json = mapper.writeValueAsString(node);

        mockMvc.perform(post("/api/survey/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isOk());
    }

    @Test
    void update() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        ObjectNode node = mapper.createObjectNode();
        node.put("surveyId", "1");
        node.put("status", "1");

        String json = mapper.writeValueAsString(node);

        mockMvc.perform(post("/api/survey/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isOk());
    }

    @Test
    void vote() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        ObjectNode node = mapper.createObjectNode();
        node.put("surveyId", "1");
        node.put("surveyResponseId", "1");
        node.put("userId", "1");

        String json = mapper.writeValueAsString(node);

        mockMvc.perform(post("/api/survey/vote")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isOk());
    }

    @Test
    void listAll() throws Exception {
        mockMvc.perform(org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get("/api/survey/list?userId=1&contribution=true")).andExpect(status().isOk());
    }

    @Test
    void listByUser() throws Exception {
        mockMvc.perform(org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get("/api/survey/list?userId=1&contribution=false")).andExpect(status().isOk());
    }
}