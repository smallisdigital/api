package com.smallisdigital.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.smallisdigital.service.NotationService;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = NotationController.class)
class NotationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private NotationService notationService;

    @Test
    void create() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        ObjectNode node = mapper.createObjectNode();
        node.put("inputId", "1");
        node.put("userId", "1");
        node.put("note", "1");
        node.put("comment", "OK");

        String json = mapper.writeValueAsString(node);

        mockMvc.perform(post("/api/notation/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk());
    }
}
