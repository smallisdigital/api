package com.smallisdigital.controller;

import com.smallisdigital.service.CategoryService;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = CategoryController.class)
class CategoryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CategoryService categoryService;

    @Test
    void getCategoryList() throws Exception {
        mockMvc.perform(get("/api/category/list")).andExpect(status().isOk());
    }

    @Test
    void getSubCategoryList() throws Exception {
        mockMvc.perform(get("/api/category/sub/list?category=1")).andExpect(status().isOk());
    }
}