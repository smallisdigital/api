package com.smallisdigital.controller;

import com.smallisdigital.service.StatisticService;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = StatisticController.class)
class StatisticControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private StatisticService statisticService;

    @Test
    void getRequestByPeriod() throws Exception {
        mockMvc.perform(org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get("/api/statistic/request?period=month")).andExpect(status().isBadRequest());
    }
}