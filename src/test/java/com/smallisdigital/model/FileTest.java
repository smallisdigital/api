package com.smallisdigital.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class FileTest {

    private final Request REQUEST = new Request();
    private final FileType FILETYPE = new FileType();
    private final byte[] DATA = new byte[1000];

    File file1, file2;

    @BeforeEach
    void setUp() {
        file1 = new File();
        file2 = new File(REQUEST, FILETYPE, DATA);
    }

    @Test
    void getterTest() {
        assertNull(file1.getId());
        assertNull(file1.getRequest());
        assertNull(file1.getFileType());
        assertNull(file1.getData());

        assertEquals(file2.getRequest(), REQUEST);
        assertEquals(file2.getFileType(), FILETYPE);
        assertEquals(file2.getData(), DATA);
    }
}
