package com.smallisdigital.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class CategoryTest {

    private final int ID = 1;
    private final String NAME = "Catégorie 1";

    Category category1, category2;

    @BeforeEach
    void setUp() {
        category1 = new Category();
        category2 = new Category(ID, NAME);
    }

    @Test
    void getterTest() {
        assertNull(category1.getId());
        assertNull(category1.getName());

        assertEquals(category2.getId(), ID);
        assertEquals(category2.getName(), NAME);
    }
}
