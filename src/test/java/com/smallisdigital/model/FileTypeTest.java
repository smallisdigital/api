package com.smallisdigital.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class FileTypeTest {

    private final int ID = 1;
    private final String NAME = "Picture";

    FileType fileType1, fileType2;

    @BeforeEach
    void setUp() {
        fileType1 = new FileType();
        fileType2 = new FileType(ID, NAME);
    }

    @Test
    void getterTest() {
        assertNull(fileType1.getId());
        assertNull(fileType1.getName());

        assertEquals(fileType2.getId(), ID);
        assertEquals(fileType2.getName(), NAME);
    }
}
