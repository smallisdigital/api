package com.smallisdigital.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

class SubCategoryTest {

    private final int ID = 1;
    private final String NAME = "Sous-catégorie 1";
    private final Category CATEGORY = new Category();

    SubCategory subCategory1, subCategory2;

    @BeforeEach
    void setUp() {
        subCategory1 = new SubCategory();
        subCategory2 = new SubCategory(ID, NAME, CATEGORY);
    }

    @Test
    void getterTest() {
        assertNull(subCategory1.getId());
        assertNull(subCategory1.getName());
        assertNull(subCategory1.getCategory());

        assertEquals(subCategory2.getId(), ID);
        assertEquals(subCategory2.getName(), NAME);
        assertEquals(subCategory2.getCategory(), CATEGORY);
    }
}
