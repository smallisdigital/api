package com.smallisdigital.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FunctionTest {

    private final User USER = new User();
    private final Category CATEGORY = new Category();
    private final SubCategory SUBCATEGORY = new SubCategory();

    Function function1, function2;

    @BeforeEach
    void setUp() {
        function1 = new Function();
        function2 = new Function(USER, CATEGORY, SUBCATEGORY);
    }

    @Test
    void getterTest() {
        assertNull(function1.getId());
        assertNull(function1.getUser());
        assertNull(function1.getCategory());
        assertNull(function1.getSubCategory());

        assertEquals(function2.getUser(), USER);
        assertEquals(function2.getCategory(), CATEGORY);
        assertEquals(function2.getSubCategory(), SUBCATEGORY);
    }
}
