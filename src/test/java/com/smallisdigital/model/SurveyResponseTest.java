package com.smallisdigital.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class SurveyResponseTest {

    private final Survey SURVEY = new Survey();
    private final String VALUE = "VALUE";

    SurveyResponse surveyResponse1, surveyResponse2;

    @BeforeEach
    void setUp() {
        surveyResponse1 = new SurveyResponse();
        surveyResponse2 = new SurveyResponse(SURVEY, VALUE);
    }

    @Test
    void getterTest() {
        assertNull(surveyResponse1.getId());
        assertNull(surveyResponse1.getSurvey());
        assertNull(surveyResponse1.getValue());

        assertEquals(surveyResponse2.getSurvey(), SURVEY);
        assertEquals(surveyResponse2.getValue(), VALUE);
    }
}
