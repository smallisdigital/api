package com.smallisdigital.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

class UserTest {
    
    private final String FIRSTNAME = "Small is Digital";
    private final String LASTNAME = "Admin";
    private final String EMAIL = "contact@smallisdigital.com";
    private final String PASSWORD = "smallisdigital";
    private final int BIRTHDAY = 1970;
    private final String PHONE = "0102030405";
    private final String ADDRESS = "4954 Par Drive";
    private final Level LEVEL = new Level();

    User user1, user2;

    @BeforeEach
    void setUp() {
        user1 = new User();
        user2 = new User(FIRSTNAME, LASTNAME, EMAIL, PASSWORD, BIRTHDAY, ADDRESS, PHONE, LEVEL);
    }

    @Test
    void getterTest() {
        assertNull(user1.getId());
        assertNull(user1.getFirstname());
        assertNull(user1.getLastname());
        assertNull(user1.getEmail());
        assertNull(user1.getPassword());
        assertNull(user1.getBirthday());
        assertNull(user1.getAddress());
        assertNull(user1.getPhone());
        assertNull(user1.getLevel());

        assertEquals(user2.getFirstname(), FIRSTNAME);
        assertEquals(user2.getLastname(), LASTNAME);
        assertEquals(user2.getEmail(), EMAIL);
        assertEquals(user2.getPassword(), PASSWORD);
        assertEquals(user2.getBirthday(), BIRTHDAY);
        assertEquals(user2.getAddress(), ADDRESS);
        assertEquals(user2.getPhone(), PHONE);
        assertEquals(user2.getLevel(), LEVEL);
    }

    @Test
    void setterTest() {
        final String NEW_FIRSTNAME = "Small is Digital New";
        final String NEW_LASTNAME = "Admi NEw";
        final String NEW_ADDRESS = "4955 Par Drive";
        final String NEW_PHONE = "0102030406";
        final Integer NEW_BIRTHDAY = 1971;

        user1.setFirstname(NEW_FIRSTNAME);
        user1.setLastname(NEW_LASTNAME);
        user1.setBirthday(NEW_BIRTHDAY);
        user1.setAddress(NEW_ADDRESS);
        user1.setPhone(NEW_PHONE);

        assertEquals(user1.getFirstname(), NEW_FIRSTNAME);
        assertEquals(user1.getLastname(), NEW_LASTNAME);
        assertEquals(user1.getBirthday(), NEW_BIRTHDAY);
        assertEquals(user1.getAddress(), NEW_ADDRESS);
        assertEquals(user1.getPhone(), NEW_PHONE);
    }
}
