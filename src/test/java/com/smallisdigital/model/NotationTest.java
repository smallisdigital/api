package com.smallisdigital.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

class NotationTest {

    private final Request REQUEST = new Request();
    private final User USER = new User();
    private final double NOTE = 2.0;
    private final int NOTE_GLOBAL = 1;
    private final int NOTE_DELAY = 2;
    private final int NOTE_QUALITY = 3;
    private final String TEXT = "No comment";
    private final Date DATE = new Date();

    Notation notation1, notation2;

    @BeforeEach
    void setUp() {
        notation1 = new Notation();
        notation2 = new Notation(REQUEST, USER, NOTE, NOTE_GLOBAL, NOTE_DELAY, NOTE_QUALITY, TEXT, DATE);
    }

    @Test
    void getterTest() {
        assertNull(notation1.getId());
        assertNull(notation1.getRequest());
        assertNull(notation1.getUser());
        assertNull(notation1.getNote());
        assertNull(notation1.getNoteGlobal());
        assertNull(notation1.getNoteDelay());
        assertNull(notation1.getNoteQuality());
        assertNull(notation1.getText());
        assertNull(notation1.getDate());

        assertEquals(notation2.getRequest(), REQUEST);
        assertEquals(notation2.getUser(), USER);
        assertEquals(notation2.getNote(), NOTE);
        assertEquals(notation2.getNoteGlobal(), NOTE_GLOBAL);
        assertEquals(notation2.getNoteDelay(), NOTE_DELAY);
        assertEquals(notation2.getNoteQuality(), NOTE_QUALITY);
        assertEquals(notation2.getText(), TEXT);
        assertEquals(notation2.getDate(), DATE);
    }
}
