package com.smallisdigital.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class SurveyVoteTest {

    private final Survey SURVEY = new Survey();
    private final SurveyResponse SURVEY_RESPONSE = new SurveyResponse();
    private final User USER = new User();

    SurveyVote surveyVote1, surveyVote2;

    @BeforeEach
    void setUp() {
        surveyVote1 = new SurveyVote();
        surveyVote2 = new SurveyVote(SURVEY, SURVEY_RESPONSE, USER);
    }

    @Test
    void getterTest() {
        assertNull(surveyVote1.getId());
        assertNull(surveyVote1.getSurvey());
        assertNull(surveyVote1.getSurveyResponse());
        assertNull(surveyVote1.getUser());

        assertEquals(surveyVote2.getSurvey(), SURVEY);
        assertEquals(surveyVote2.getSurveyResponse(), SURVEY_RESPONSE);
        assertEquals(surveyVote2.getUser(), USER);
    }
}
