package com.smallisdigital.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

class RequestTest {

    private final User USER = new User();
    private final Category CATEGORY = new Category();
    private final SubCategory SUBCATEGORY = new SubCategory();
    private final String ADDRESS = "4954 Par Drive";
    private final String DESCRIPTION = "The description";
    private final Date CREATIONDATE = new Date();
    private final Date STARTDATE = new Date();
    private final Date COMPLETEDATE = new Date();
    private final Date SCHEDULEDATE = new Date();
    private final int STATUS = 0;
    private final int PRIORITY = 1;
    private final int DELAY = 2;
    private final User CURRENT_USER = new User();

    Request request1, request2;

    @BeforeEach
    void setUp() {
        request1 = new Request();
        request2 = new Request(USER, CATEGORY, SUBCATEGORY, ADDRESS, DESCRIPTION, CREATIONDATE, STARTDATE, COMPLETEDATE, SCHEDULEDATE, STATUS, PRIORITY, DELAY, CURRENT_USER);
    }

    @Test
    void getterTest() {
        assertNull(request1.getId());
        assertNull(request1.getUser());
        assertNull(request1.getCategory());
        assertNull(request1.getSubcategory());
        assertNull(request1.getDescription());
        assertNull(request1.getCreationDate());
        assertNull(request1.getStartDate());
        assertNull(request1.getCompleteDate());
        assertNull(request1.getScheduleDate());
        assertNull(request1.getStatus());
        assertNull(request1.getPriority());
        assertNull(request1.getDelay());
        assertNull(request1.getCurrentUser());

        assertEquals(request2.getUser(), USER);
        assertEquals(request2.getCategory(), CATEGORY);
        assertEquals(request2.getSubcategory(), SUBCATEGORY);
        assertEquals(request2.getAddress(), ADDRESS);
        assertEquals(request2.getDescription(), DESCRIPTION);
        assertEquals(request2.getCreationDate(), CREATIONDATE);
        assertEquals(request2.getStartDate(), STARTDATE);
        assertEquals(request2.getCompleteDate(), COMPLETEDATE);
        assertEquals(request2.getScheduleDate(), SCHEDULEDATE);
        assertEquals(request2.getStatus(), STATUS);
        assertEquals(request2.getPriority(), PRIORITY);
        assertEquals(request2.getDelay(), DELAY);
        assertEquals(request2.getCurrentUser(), CURRENT_USER);
    }

    @Test
    void setterTest() {
        final Integer NEW_STATUS = 1;
        final Integer NEW_PRIORITY = 2;
        final Integer NEW_DELAY = 3;
        final User NEW_USER = new User();
        final Date NEW_STARTDATE = new Date();
        final Date NEW_COMPLETEDATE = new Date();
        final Date NEW_SCHEDULEDATE = new Date();

        request1.setStatus(NEW_STATUS);
        request1.setPriority(NEW_PRIORITY);
        request1.setDelay(NEW_DELAY);
        request1.setCurrentUser(NEW_USER);
        request1.setStartDate(NEW_STARTDATE);
        request1.setCompleteDate(NEW_COMPLETEDATE);
        request1.setScheduleDate(NEW_SCHEDULEDATE);

        assertEquals(request1.getStatus(), NEW_STATUS);
        assertEquals(request1.getPriority(), NEW_PRIORITY);
        assertEquals(request1.getDelay(), NEW_DELAY);
        assertEquals(request1.getCurrentUser(), NEW_USER);
        assertEquals(request1.getStartDate(), NEW_STARTDATE);
        assertEquals(request1.getCompleteDate(), NEW_COMPLETEDATE);
        assertEquals(request1.getScheduleDate(), NEW_SCHEDULEDATE);
    }
}
