package com.smallisdigital.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

class LevelTest {

    private final String NAME = "Citoyen";

    Level level1, level2;

    @BeforeEach
    void setUp() {
        level1 = new Level();
        level2 = new Level(NAME);
    }

    @Test
    void getterTest() {
        assertNull(level1.getId());
        assertNull(level1.getName());

        assertEquals(level2.getName(), NAME);
    }
}
