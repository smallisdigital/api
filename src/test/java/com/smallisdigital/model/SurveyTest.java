package com.smallisdigital.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class SurveyTest {

    private final User USER = new User();
    private final Date CREATION_DATE = new Date();
    private final Integer STATUS = 1;
    private final String NAME = "NAME";
    private final String QUESTION = "QUESTION";
    private final String DESCRIPTION = "DESCRIPTION";

    Survey survey1, survey2;

    @BeforeEach
    void setUp() {
        survey1 = new Survey();
        survey2 = new Survey(USER, CREATION_DATE, STATUS, NAME, QUESTION, DESCRIPTION);
    }

    @Test
    void getterTest() {
        assertNull(survey1.getId());
        assertNull(survey1.getUser());
        assertNull(survey1.getCreationDate());
        assertNull(survey1.getStatus());
        assertNull(survey1.getName());
        assertNull(survey1.getQuestion());
        assertNull(survey1.getDescription());

        assertEquals(survey2.getUser(), USER);
        assertEquals(survey2.getCreationDate(), CREATION_DATE);
        assertEquals(survey2.getStatus(), STATUS);
        assertEquals(survey2.getName(), NAME);
        assertEquals(survey2.getQuestion(), QUESTION);
        assertEquals(survey2.getDescription(), DESCRIPTION);
    }

    @Test
    void setterTest() {
        final Integer NEW_STATUS = 1;

        survey1.setStatus(NEW_STATUS);

        assertEquals(survey1.getStatus(), NEW_STATUS);
    }
}
