package com.smallisdigital.dto;

import com.smallisdigital.model.Category;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CategoryDtoTest {

    private final boolean SUCCESS = true;
    private final List<Category> CATEGORIES = Arrays.asList(new Category(1, "Catégorie 1"), new Category(2, "Catégorie 2"));

    CategoryDto categoryDto;

    @BeforeEach
    void setUp() {
        categoryDto = new CategoryDto(SUCCESS, CATEGORIES);
    }

    @Test
    void recordTest() {
        assertEquals(categoryDto.success(), SUCCESS);
        assertEquals(categoryDto.category(), CATEGORIES);
    }
}
