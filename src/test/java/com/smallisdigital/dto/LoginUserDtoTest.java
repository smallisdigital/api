package com.smallisdigital.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LoginUserDtoTest {

    private final boolean SUCCESS = true;
    private final int ID = 1;
    private final String FIRSTNAME = "Small is Digital";
    private final String LASTNAME = "Admin";
    private final int LEVEL = 1;
    private final int CATGEORY_ID = 1;
    private final String CATEGORY_NAME = "Catégorie";
    private final int SUBCATEGORY_ID = 1;
    private final String SUBCATEGORY_NAME = "Sous-Catégorie";
    private final String ERROR = "No content";

    LoginUserDto loginUserDto;

    @BeforeEach
    void setUp() {
        loginUserDto = new LoginUserDto(SUCCESS, ID, FIRSTNAME, LASTNAME, LEVEL, CATGEORY_ID, CATEGORY_NAME, SUBCATEGORY_ID, SUBCATEGORY_NAME, ERROR);
    }

    @Test
    void recordTest() {
        assertEquals(loginUserDto.success(), SUCCESS);
        assertEquals(loginUserDto.id(), ID);
        assertEquals(loginUserDto.firstname(), FIRSTNAME);
        assertEquals(loginUserDto.lastname(), LASTNAME);
        assertEquals(loginUserDto.level(), LEVEL);
        assertEquals(loginUserDto.categoryId(), CATGEORY_ID);
        assertEquals(loginUserDto.categoryName(), CATEGORY_NAME);
        assertEquals(loginUserDto.subCategoryId(), SUBCATEGORY_ID);
        assertEquals(loginUserDto.subCategoryName(), SUBCATEGORY_NAME);
        assertEquals(loginUserDto.error(), ERROR);
    }
}
