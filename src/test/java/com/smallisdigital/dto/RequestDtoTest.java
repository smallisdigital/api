package com.smallisdigital.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RequestDtoTest {

    private final int INPUTID = 1000;
    private final String CATEGORY = "Catégorie 1";
    private final String SUBCATEGORY = "Sous-catégorie 1";
    private final String CREATIONDATE = "1970-01-01";
    private final String STARTDATE = "1970-01-02";
    private final String SCHEDULEDATE = "1970-01-03";
    private final String COMPLETEDATE = "1970-01-04";
    private final String IMAGE = "IMAGE";
    private final String ADDRESS = "4954 Par Drive";
    private final String DESCRIPTION = "No content";
    private final int STATUS = 1;
    private final int PRIORITY = 1;
    private final int DELAY = 1;
    private final double NOTE = 2.0;
    private final int NOTE_GLOBAL = 1;
    private final int NOTE_DELAY = 2;
    private final int NOTE_QUALITY = 3;

    RequestDto requestDto;

    @BeforeEach
    void setUp() {
        requestDto = new RequestDto(INPUTID, CATEGORY, SUBCATEGORY, CREATIONDATE, STARTDATE, SCHEDULEDATE, COMPLETEDATE, IMAGE, ADDRESS, DESCRIPTION, STATUS, PRIORITY, DELAY, NOTE, NOTE_GLOBAL, NOTE_DELAY, NOTE_QUALITY);
    }

    @Test
    void recordTest() {
        assertEquals(requestDto.inputId(), INPUTID);
        assertEquals(requestDto.category(), CATEGORY);
        assertEquals(requestDto.subcategory(), SUBCATEGORY);
        assertEquals(requestDto.creationDate(), CREATIONDATE);
        assertEquals(requestDto.startDate(), STARTDATE);
        assertEquals(requestDto.scheduleDate(), SCHEDULEDATE);
        assertEquals(requestDto.completeDate(), COMPLETEDATE);
        assertEquals(requestDto.image(), IMAGE);
        assertEquals(requestDto.address(), ADDRESS);
        assertEquals(requestDto.description(), DESCRIPTION);
        assertEquals(requestDto.status(), STATUS);
        assertEquals(requestDto.priority(), PRIORITY);
        assertEquals(requestDto.delay(), DELAY);
        assertEquals(requestDto.note(), NOTE);
        assertEquals(requestDto.noteGlobal(), NOTE_GLOBAL);
        assertEquals(requestDto.noteDelay(), NOTE_DELAY);
        assertEquals(requestDto.noteQuality(), NOTE_QUALITY);
    }
}
