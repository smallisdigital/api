package com.smallisdigital.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UpdateUserDtoTest {

    private final String FIRSTNAME = "Small is Digital";
    private final String LASTNAME = "Admin";
    private final String EMAIL = "contact@smallisdigital.com";
    private final String PHONE = "0102030405";
    private final String BIRTHDAY = "1970";
    private final String ADDRESS = "4954 Par Drive";

    UpdateUserDto updateUserDto;

    @BeforeEach
    void setUp() {
        updateUserDto = new UpdateUserDto(FIRSTNAME, LASTNAME, EMAIL, PHONE, BIRTHDAY, ADDRESS);
    }

    @Test
    void recordTest() {
        assertEquals(updateUserDto.firstname(), FIRSTNAME);
        assertEquals(updateUserDto.lastname(), LASTNAME);
        assertEquals(updateUserDto.email(), EMAIL);
        assertEquals(updateUserDto.phone(), PHONE);
        assertEquals(updateUserDto.birthday(), BIRTHDAY);
        assertEquals(updateUserDto.address(), ADDRESS);
    }
}
