package com.smallisdigital.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CreateRequestDtoTest {

    private final int USER = 1;
    private final int CATEGORY = 1;
    private final int SUBCATEGORY = 1;
    private final String ADDRESS = "ADDRESS";
    private final String DESCRIPTION = "DESCRIPTION";
    private final String IMAGE = "IMAGE";

    CreateRequestDto createRequestDto;

    @BeforeEach
    void setUp() {
        createRequestDto = new CreateRequestDto(USER, CATEGORY, SUBCATEGORY, ADDRESS, DESCRIPTION, IMAGE);
    }

    @Test
    void recordTest() {
        assertEquals(createRequestDto.user(), USER);
        assertEquals(createRequestDto.category(), CATEGORY);
        assertEquals(createRequestDto.subcategory(), SUBCATEGORY);
        assertEquals(createRequestDto.address(), ADDRESS);
        assertEquals(createRequestDto.description(), DESCRIPTION);
        assertEquals(createRequestDto.image(), IMAGE);
    }
}
