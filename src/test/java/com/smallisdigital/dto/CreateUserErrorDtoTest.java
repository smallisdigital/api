package com.smallisdigital.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CreateUserErrorDtoTest {

    private final String NAME = "NAME";
    private final String VALUE = "VALUE";

    CreateUserErrorDto createUserErrorDto;

    @BeforeEach
    void setUp() {
        createUserErrorDto = new CreateUserErrorDto(NAME, VALUE);
    }

    @Test
    void recordTest() {
        assertEquals(createUserErrorDto.name(), NAME);
        assertEquals(createUserErrorDto.value(), VALUE);
    }
}
