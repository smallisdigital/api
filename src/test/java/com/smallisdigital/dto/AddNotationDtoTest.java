package com.smallisdigital.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AddNotationDtoTest {

    private final int INPUTID = 1000;
    private final int USERID = 1;
    private final int NOTE_GLOBAL = 1;
    private final int NOTE_DELAY = 2;
    private final int NOTE_QUALITY = 3;
    private final String COMMENT = "COMMENTAIRE";

    AddNotationDto addNotationDto;

    @BeforeEach
    void setUp() {
        addNotationDto = new AddNotationDto(INPUTID, USERID, NOTE_GLOBAL, NOTE_DELAY, NOTE_QUALITY, COMMENT);
    }

    @Test
    void recordTest() {
        assertEquals(addNotationDto.inputId(), INPUTID);
        assertEquals(addNotationDto.userId(), USERID);
        assertEquals(addNotationDto.noteGlobal(), NOTE_GLOBAL);
        assertEquals(addNotationDto.noteDelay(), NOTE_DELAY);
        assertEquals(addNotationDto.noteQuality(), NOTE_QUALITY);
        assertEquals(addNotationDto.comment(), COMMENT);
    }
}
