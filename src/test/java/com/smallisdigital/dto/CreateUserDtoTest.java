package com.smallisdigital.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CreateUserDtoTest {

    private final String FIRSTNAME = "Small is Digital";
    private final String LASTNAME = "Admin";
    private final String EMAIL = "contact@smallisdigital.com";
    private final String PASSWORD = "smallisdigital";
    private final String REPASSWORD = "smallisdigital";

    CreateUserDto createUserDto;

    @BeforeEach
    void setUp() {
        createUserDto = new CreateUserDto(FIRSTNAME, LASTNAME, EMAIL, PASSWORD, REPASSWORD);
    }

    @Test
    void recordTest() {
        assertEquals(createUserDto.firstname(), FIRSTNAME);
        assertEquals(createUserDto.lastname(), LASTNAME);
        assertEquals(createUserDto.email(), EMAIL);
        assertEquals(createUserDto.password(), PASSWORD);
        assertEquals(createUserDto.repassword(), REPASSWORD);
    }
}
