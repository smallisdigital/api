package com.smallisdigital.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CreateUserStatusDtoTest {

    private final boolean SUCCESS = false;
    private final List<CreateUserErrorDto> ERROR = new ArrayList<>();

    CreateUserStatusDto createUserStatusDto;

    @BeforeEach
    void setUp() {
        createUserStatusDto = new CreateUserStatusDto(SUCCESS, ERROR);
    }

    @Test
    void recordTest() {
        assertEquals(createUserStatusDto.success(), SUCCESS);
        assertEquals(createUserStatusDto.error(), ERROR);
    }
}
