package com.smallisdigital.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class VoteSurveyDtoTest {

    private final int SURVEY_ID = 1;
    private final int SURVEY_RESPONSE_ID = 1;
    private final int USER_ID = 1;

    VoteSurveyDto voteSurveyDto;

    @BeforeEach
    void setUp() {
        voteSurveyDto = new VoteSurveyDto(SURVEY_ID, SURVEY_RESPONSE_ID, USER_ID);
    }

    @Test
    void recordTest() {
        assertEquals(voteSurveyDto.surveyId(), SURVEY_ID);
        assertEquals(voteSurveyDto.surveyResponseId(), SURVEY_RESPONSE_ID);
        assertEquals(voteSurveyDto.userId(), USER_ID);
    }
}
