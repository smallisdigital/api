package com.smallisdigital.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ListSurveyDtoTest {

    private final int SURVEY_ID = 1;
    private final String NAME = "NAME";
    private final String QUESTION = "QUESTION";
    private final List<String> RESPONSE = new ArrayList<>();
    private final List<Float> VOTE = new ArrayList<>();
    private final int STATUS = 1;
    private final Integer CONTRIBUTION = 1;

    ListSurveyDto listSurveyDto;

    @BeforeEach
    void setUp() {
        listSurveyDto = new ListSurveyDto(SURVEY_ID, NAME, QUESTION, RESPONSE, VOTE, STATUS, CONTRIBUTION);
    }

    @Test
    void recordTest() {
        assertEquals(listSurveyDto.surveyId(), SURVEY_ID);
        assertEquals(listSurveyDto.name(), NAME);
        assertEquals(listSurveyDto.question(), QUESTION);
        assertEquals(listSurveyDto.response(), RESPONSE);
        assertEquals(listSurveyDto.vote(), VOTE);
        assertEquals(listSurveyDto.status(), STATUS);
        assertEquals(listSurveyDto.contribution(), CONTRIBUTION);
    }
}
