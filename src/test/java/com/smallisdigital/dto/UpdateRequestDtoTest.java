package com.smallisdigital.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UpdateRequestDtoTest {

    private final int INPUTID = 1;
    private final int USERID = 2;
    private final int PRIORITY = 3;
    private final int DELAY = 4;
    private final String SCHEDULEDATE = "DD/MM/YYYY";

    UpdateRequestDto updateRequestDto;

    @BeforeEach
    void setUp() {
        updateRequestDto = new UpdateRequestDto(INPUTID, USERID, PRIORITY, DELAY, SCHEDULEDATE);
    }

    @Test
    void recordTest() {
        assertEquals(updateRequestDto.inputId(), INPUTID);
        assertEquals(updateRequestDto.userId(), USERID);
        assertEquals(updateRequestDto.priority(), PRIORITY);
        assertEquals(updateRequestDto.delay(), DELAY);
        assertEquals(updateRequestDto.scheduleDate(), SCHEDULEDATE);
    }
}
