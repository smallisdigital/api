package com.smallisdigital.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ListRequestDtoTest {

    private final int INPUTID = 1000;
    private final String CATEGORY = "Catégorie 1";
    private final String SUBCATEGORY = "Catégorie 2";
    private final String CREATIONDATE = "1970-01-01";
    private final String SCHEDULEDATE = "1970-01-03";
    private final String ADDRESS = "4954 Par Drive";
    private final int PRIORITY = 1;
    private final int DELAY = 1;
    private final int STATUS = 1;
    private final boolean NOTE = true;

    ListRequestDto listRequestDto;

    @BeforeEach
    void setUp() {
        listRequestDto = new ListRequestDto(INPUTID, CATEGORY, SUBCATEGORY, CREATIONDATE, SCHEDULEDATE, ADDRESS, PRIORITY, DELAY, STATUS, NOTE);
    }

    @Test
    void recordTest() {
        assertEquals(listRequestDto.inputId(), INPUTID);
        assertEquals(listRequestDto.category(), CATEGORY);
        assertEquals(listRequestDto.subcategory(), SUBCATEGORY);
        assertEquals(listRequestDto.creationDate(), CREATIONDATE);
        assertEquals(listRequestDto.scheduleDate(), SCHEDULEDATE);
        assertEquals(listRequestDto.address(), ADDRESS);
        assertEquals(listRequestDto.priority(), PRIORITY);
        assertEquals(listRequestDto.delay(), DELAY);
        assertEquals(listRequestDto.status(), STATUS);
        assertEquals(listRequestDto.note(), NOTE);
    }
}
