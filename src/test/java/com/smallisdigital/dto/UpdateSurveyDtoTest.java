package com.smallisdigital.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UpdateSurveyDtoTest {

    private final int SURVEY_ID = 1;
    private final int STATUS = 1;

    UpdateSurveyDto updateSurveyDto;

    @BeforeEach
    void setUp() {
        updateSurveyDto = new UpdateSurveyDto(SURVEY_ID, STATUS);
    }

    @Test
    void recordTest() {
        assertEquals(updateSurveyDto.surveyId(), SURVEY_ID);
        assertEquals(updateSurveyDto.status(), STATUS);
    }
}
