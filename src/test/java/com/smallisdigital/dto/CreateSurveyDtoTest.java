package com.smallisdigital.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CreateSurveyDtoTest {

    private final int USER = 1;
    private final String NAME = "NAME";
    private final String QUESTION = "QUESTION";
    private final String DESCRIPTION = "DESCRIPTION";
    private final List<String> RESPONSE = new ArrayList<>();

    CreateSurveyDto createSurveyDto;

    @BeforeEach
    void setUp() {
        createSurveyDto = new CreateSurveyDto(USER, NAME, QUESTION, DESCRIPTION, RESPONSE);
    }

    @Test
    void recordTest() {
        assertEquals(createSurveyDto.user(), USER);
        assertEquals(createSurveyDto.name(), NAME);
        assertEquals(createSurveyDto.question(), QUESTION);
        assertEquals(createSurveyDto.description(), DESCRIPTION);
        assertEquals(createSurveyDto.response(), RESPONSE);
    }
}
