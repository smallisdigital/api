package com.smallisdigital.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GetUserDtoTest {

    private final String FIRSTNAME = "Small is Digital";
    private final String LASTNAME = "Admin";
    private final String LEVEL = "1";
    private final String EMAIL = "contact@smallisdigital.com";
    private final String PHONE = "0102030405";
    private final int BIRTHDAY = 1970;
    private final String ADDRESS = "4954 Par Drive";

    GetUserDto getUserDto;

    @BeforeEach
    void setUp() {
        getUserDto = new GetUserDto(FIRSTNAME, LASTNAME, LEVEL, EMAIL, PHONE, BIRTHDAY, ADDRESS);
    }

    @Test
    void recordTest() {
        assertEquals(getUserDto.firstname(), FIRSTNAME);
        assertEquals(getUserDto.lastname(), LASTNAME);
        assertEquals(getUserDto.level(), LEVEL);
        assertEquals(getUserDto.email(), EMAIL);
        assertEquals(getUserDto.phone(), PHONE);
        assertEquals(getUserDto.birthday(), BIRTHDAY);
        assertEquals(getUserDto.address(), ADDRESS);
    }
}
