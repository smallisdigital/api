package com.smallisdigital.dto;

import com.smallisdigital.model.Category;
import com.smallisdigital.model.SubCategory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SubCategoryDtoTest {

    private final boolean SUCCESS = true;
    private final List<SubCategory> SUBCATEGORIES = Arrays.asList(new SubCategory(1, "Sous-catégorie 1", new Category(1, "Catégorie 1")), new SubCategory(2, "Sous-catégorie 2", new Category(2, "Catégorie 2")));

    SubCategoryDto subCategoryDto;

    @BeforeEach
    void setUp() {
        subCategoryDto = new SubCategoryDto(SUCCESS, SUBCATEGORIES);
    }

    @Test
    void recordTest() {
        assertEquals(subCategoryDto.success(), SUCCESS);
        assertEquals(subCategoryDto.subCategories(), SUBCATEGORIES);
    }
}
