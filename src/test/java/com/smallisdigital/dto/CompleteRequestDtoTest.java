package com.smallisdigital.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CompleteRequestDtoTest {

    private final int INPUTID = 1;

    CompleteRequestDto completeRequestDto;

    @BeforeEach
    void setUp() {
        completeRequestDto = new CompleteRequestDto(INPUTID);
    }

    @Test
    void recordTest() {
        assertEquals(completeRequestDto.inputId(), INPUTID);
    }
}
